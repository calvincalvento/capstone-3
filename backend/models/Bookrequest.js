const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BookrequestSchema = new Schema({

  bookerId: String,
  daterId: String,
  date: Date,
  location:{
    locationName:String,
    latitude: String,
    longhitude: String
  },
  time:{
    startTime: String,
    endTime: String
  },
  price: Number,
  created_at: {type:Date, default: Date.now()},
  status : {type:String, default: "pending"}

})

const Bookrequest = mongoose.model('Bookrequest', BookrequestSchema);

module.exports = Bookrequest;