const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({

  name:{
    firstName: String,
    lastName: String
  },
  
  email: String,
  username: String,
  password: String,
  profilePic: [
		{
			pictureName: String,
			created_at: {type:Date, default: Date.now()}
		
    }],
  aboutMe:
    {
      height: Number,
      weight: Number,
      relationship: String,
      smoke: String,
      drink: String,
      work: String,
      city: String,
      country: String
    },
    verification:{
      address:{
        blk: String,
        city: String,
        province: String
      },
      birthday: Date,
      age: Number,
      contact: Number,
      gender: String,
      nationality: String,
      validId: String

    },
    lookingFor:{
        gender: String,
        description: String

    },
    myInterest:[{
      interestName:String,
      interestIcon: String
    }],

  created_at: {type:Date, default: Date.now()},
  status : {type:String, default: "user-nonverified"}




})

const User = mongoose.model('User', UserSchema);

module.exports = User;