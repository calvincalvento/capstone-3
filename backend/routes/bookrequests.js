const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const BookrequestModel = require('../models/Bookrequest');
const UserModel = require('../models/User');


router.get('/',async (req,res) => {

	let user = await BookrequestModel.find();

	res.send(user);


})

 

router.get('/:id',async (req,res) => {

	let bookrequest = await BookrequestModel.findById(req.params.id);

	res.send(user);


})

router.post('/',async (req,res) => {

	let bookrequest1 = new BookrequestModel({

    bookerId: req.body.bookerId,
    daterId: req.body.daterId,
    date:req.body.date,
    price:req.body.price,
    location:{
      locationName:req.body.locationName,
      latitude: req.body.latitude,
      longhitude:req.body.longhitude
    },
    time:{
      startTime:req.body.startTime,
      endTime:req.body.endTime
    },
   
  })

  let bookrequest2 = await bookrequest1.save();
 
	res.send(bookrequest2);


})

router.patch('/:id/pendingConfirm', async( req,res) => {


  let pendingConfirm = await BookrequestModel.findByIdAndUpdate(req.params.id, {status:req.body.pendingConfirm}, {new: true}); 
  
  



  
  res.send(pendingConfirm);
  
  });

  router.patch('/:id/viewProfileConfirm', async( req,res) => {


    let pendingConfirm = await BookrequestModel.findByIdAndUpdate(req.params.id, {status:req.body.pendingConfirm}, {new: true}); 
    
    let user = await UserModel.findById(req.body.bookerId);
  
  
  
    
    res.send({users:user, requests:pendingConfirm});
    
    });

module.exports = router

