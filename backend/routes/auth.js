const bcrypt = require('bcrypt');
const  express = require ('express');
const router = express.Router();
const UserModel = require('../models/User');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const config = require('../config');

router.post('/', async(req,res)=>{

	let user = await UserModel.findOne({ email:req.body.email});

	 

	 if(!user){

	 	return res.status(400).send({
	 		message: "Email or Password is invalid"
	 	});

	 }

let iscompared = await bcrypt.compare(req.body.password, user.password);

	 if(!iscompared){

	 	return res.status(400).send({
	 		message: "Email or Password Does not Match"


	 	});

	 }

	const payload =
	{_id: user.id, email:user.email, fullname: `${user.name.firstName} ${user.name.lastName}`, type:user.name.type};
	 // generate token
	 const token = jwt.sign(payload, config.secret);
	 let userall = await UserModel.find();
	res.header('x-auth-token', token).send({login: user, users: userall}); // attach the token to the header

});

module.exports = router