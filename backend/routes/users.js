const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const UserModel = require('../models/User');
const BookrequestModel = require('../models/Bookrequest');
const bcrypt =require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config');



router.get('/',async (req,res) => {

  let user = await UserModel.find();
  
  let request = await BookrequestModel.find();

	res.send({users:user, requests:request});


})

 

router.get('/:id',async (req,res) => {

	let user = await UserModel.findById(req.params.id);

	res.send(user);


})

// myprofile page



router.post('/',async (req,res) => {

  const salt = await bcrypt.genSalt(10);
  const Password = await bcrypt.hash(req.body.password, salt);

    
    var task1 = new UserModel({ 
    
      name:{
        firstName:req.body.firstName,
        lastName:req.body.lastName
      },
      email: req.body.email,
      username:req.body.username,
      password:Password
      });      
       
    let task2 = await task1.save();
    const payload =
	{_id: task2.id, email:task2.email, fullname: `${task2.name.firstName} ${task2.name.lastName}`, type:task2.name.type};
	 // generate token
	 const token = jwt.sign(payload, config.secret);

   let userall = await UserModel.find();
	res.header('x-auth-token', token).send({login: task2, users: userall});

  });

  // upload profile

  router.patch('/:id/profilePic', async( req,res) => {


    let profilePic = await UserModel.findByIdAndUpdate(req.params.id, {$push:{profilePic: {pictureName:req.body.profilePic}}}, {new: true}); 
    
    
	let user = await UserModel.findById(req.params.id);

	
    
    res.send({profileupdate: profilePic, user:user});
    
    });
      // edit About me

  router.patch('/:id/editAbout', async( req,res) => {


    let profilePic = await UserModel.findByIdAndUpdate(req.params.id, {aboutMe:
      {height:req.body.Height,
        weight:req.body.Weight,
        relationship: req.body.Relationship,
        smoke: req.body.Smoke,
        drink: req.body.Drink,
        work: req.body.Work,
        city: req.body.City,
        country: req.body.Country

       }}, {new: true}); 
    
    
	res.send(profilePic)
    
    });
    //edit looking for
    
  router.patch('/:id/lookingFor', async( req,res) => {


    let profilePic = await UserModel.findByIdAndUpdate(req.params.id, {lookingFor:
      {gender:req.body.Gender,
        description:req.body.Description,
       

       }}, {new: true}); 
    
    
	res.send(profilePic)
    
    });
    // edit my interest
    
    router.patch('/:id/myInterest', async( req,res) => {


      let profilePic = await UserModel.findByIdAndUpdate(req.params.id, {$push: {myInterest:
        {interestName:req.body.Interest
         
         
  
         }}}, {new: true}); 
      
      
    res.send(profilePic)
      
      });

// verify account
      router.patch('/:id/verifyAccount', async( req,res) => {


        let verifyAccount = await UserModel.findByIdAndUpdate(req.params.id,{name:{
          firstName:req.body.firstName,
          lastName:req.body.lastName
        }}, {new: true}); 
        
        let verifyAccount2 = await UserModel.findByIdAndUpdate(req.params.id,{verification:{
           address:{
             blk:req.body.Blk,
             city:req.body.City,
             province:req.body.Province
           },
          age:req.body.Age,
           contact:req.body.Contact,
           birthday:req.body.Birthday,
           gender:req.body.Gender,
           nationality:req.body.Nationality,
           validId:req.body.validId
        }}, {new: true}); 
      res.send(verifyAccount)

      res.send({profileupdate: verifyAccount, user:verifyAccount2});
        
        });

          // edit my interest
    
    router.patch('/:id/approveUser', async( req,res) => {


      let approveUser = await UserModel.findByIdAndUpdate(req.params.id,{status:req.body.approveUser
        }, {new: true}); 
      
      
    res.send(approveUser);
      
      });

      
    
  
  module.exports = router


 