const express =require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const config = require('./config');
const users = require('./routes/users')
const auth = require('./routes/auth');
const bookrequests = require('./routes/bookrequests');
app.use(cors());

app.listen(config.port, () =>{

  console.log(`listening to port: ${config.port}`);
});

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/bookrequests', bookrequests);

  mongoose.connect(config.database,{ useNewUrlParser: true } ).then(() =>{

    console.log('remote connection established');
    })
